import React from 'react';
import './App.css';
import Routes from './routes';

function App() {
  return (
    <div style={{ background: '#2676bf' }} className="App">
      <Routes />
    </div>
  );
}

export default App;
