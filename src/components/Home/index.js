import React, { Component } from 'react';
import {
  movieListing,
  movieListingPagination,
  SetDefault,
  pushShortlistItem,
} from '../../actions/moviesListing';
import { connect } from 'react-redux';

class index extends Component {
  state = {
    searchParam: 'all',
    pageNo: 1,
    limit: 12,
    shortlistedItem: [],
  };

  componentDidMount() {
    if (!localStorage.getItem('shortlist')) {
      localStorage.setItem('shortlist', JSON.stringify([]));
    } else {
      let array = JSON.parse(localStorage.getItem('shortlist'));
      this.props.SetDefault(array);
    }
    const { searchParam, pageNo, limit } = this.state;
    this.props.movieListing(searchParam, pageNo, limit);
  }

  pagination = () => {
    let { searchParam, pageNo, limit } = this.state;
    let page = pageNo + 1;
    this.setState({ pageNo: page }, () =>
      this.props.movieListingPagination(searchParam, this.state.pageNo, limit)
    );
  };
  searchItem = (e) => {
    e.preventDefault();
    this.setState({ pageNo: 1 }, () =>
      this.props.movieListing(
        this.state.searchParam,
        this.state.pageNo,
        this.state.limit
      )
    );
    window.scrollTo(0, 0);
  };
  shortListItem = (e, id) => {
    e.preventDefault();
    let array = JSON.parse(localStorage.getItem('shortlist'));
    array.push(id);
    this.props.pushShortlistItem(id);
    localStorage.setItem('shortlist', JSON.stringify(array));
    console.log(localStorage.getItem('shortlist'));
    // this.forceUpdate();
  };

  render() {
    console.log(this.props.shortlist, 'from');
    return (
      <div>
        <nav
          style={{ background: '#2676bf' }}
          className="navbar navbar-expand-md navbar-dark fixed-top  p-5"
        >
          <div style={{ width: '100vw' }}>
            <form
              onSubmit={(e) => this.searchItem(e)}
              style={{
                width: '100%',
                textAlign: 'center',
                display: 'block',
                border: '0px solid black ',
              }}
              className="form-inline mt-2 mt-md-0"
            >
              <input
                onChange={(e) => this.setState({ searchParam: e.target.value })}
              />
              <button
                onClick={(e) => this.searchItem(e)}
                className="buttonCustom"
                type="submit"
              >
                Go
              </button>
            </form>
          </div>
        </nav>

        <div style={{ marginTop: '12rem' }} className="row ml-5 ">
          {this.props.data.map((item, i) => {
            return (
              <div className="col-3 mb-3" key={i}>
                <div
                  className="card h-100"
                  style={{
                    width: '18rem',

                    borderRadius: '20px',
                  }}
                >
                  <div className="card-header ">
                    {this.props.shortlist.indexOf(item.mal_id) !== -1 ? (
                      <button className="btn btn-success" disabled={true}>
                        Shorlisted
                      </button>
                    ) : (
                      <button
                        className="btn btn-success"
                        onClick={(e) => this.shortListItem(e, item.mal_id)}
                      >
                        Shortlist
                      </button>
                    )}
                  </div>
                  <img
                    src={item.image_url}
                    style={{ height: '18rem' }}
                    className="card-img-top"
                    alt="..."
                  />

                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                    }}
                    className="card-body"
                  >
                    <h5
                      style={{
                        // whiteSpace: 'nowrap',
                        // width: '100%',
                        // overflow: 'hidden',
                        textOverflow: '',
                        padding: 'auto',
                      }}
                      className="card-title"
                    >
                      {/* {item.title.substring(0, 15) + '....'} */}
                      {item.title}
                    </h5>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        <div className="row text-center mt-2">
          <div className="col">
            {this.props.isData ? (
              <button
                className="btn btn-outline-warning mb-2"
                onClick={(e) => this.pagination(e)}
              >
                Load more
              </button>
            ) : (
              <p>No Records Found</p>
            )}
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    data: state.homeListReducer.data,
    isData: state.homeListReducer.isData,
    shortlist: state.homeListReducer.shortlistedItem,
  };
};
export default connect(mapStateToProps, {
  movieListing,
  movieListingPagination,
  SetDefault,
  pushShortlistItem,
})(index);
