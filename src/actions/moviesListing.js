import axios from 'axios';

export const movieListing = (searchParam, PageNo, limit) => {
  return (dispatch) => {
    axios
      .get(
        `https://api.jikan.moe/v3/search/anime?q=${searchParam}&limit=${limit}&page=${PageNo}`
      )
      .then((result) => {
        if (result.status === 200) {
          dispatch({
            type: 'MOVIE_DATA',
            payload: { result: result.data.results, isData: true },
          });
        }
      })
      .catch((err) => {
        dispatch({
          type: 'MOVIE_DATA',
          payload: { result: [], isData: false },
        });
      });
  };
};

export const movieListingPagination = (searchParam, PageNo, limit) => {
  return (dispatch) => {
    axios
      .get(
        `https://api.jikan.moe/v3/search/anime?q=${searchParam}&limit=${limit}&page=${PageNo}`
      )
      .then((result) => {
        dispatch({
          type: 'PAGINATION',
          payload: { result: result.data.results, isData: true },
        });
      })
      .catch((err) => {
        dispatch({
          type: 'PAGINATION',
          payload: { result: [], isData: false },
        });
      });
  };
};

export const SetDefault = (data) => {
  return (dispatch) => {
    dispatch({
      type: 'SETSHORTLIST',
      payload: data,
    });
  };
};

export const pushShortlistItem = (data) => {
  return (dispatch) => {
    dispatch({
      type: 'PUSHDATA',
      payload: data,
    });
  };
};
