let initialState = {
  data: [],
  shortlistedItem: [],
  isData: true,
};
const homeListReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'MOVIE_DATA':
      return {
        ...state,
        data: payload.result,
        isData: payload.isData,
      };
    case 'PAGINATION':
      let temData = [...state.data];
      if (payload.result.length) {
        temData.push(payload.result);
      }

      return {
        ...state,
        data: temData.flat(),
        isData: payload.isData,
      };
    case 'SETSHORTLIST':
      return {
        ...state,
        shortlistedItem: payload,
      };
    case 'PUSHDATA':
      console.log(payload, 'from reducer');
      let newData = [...state.shortlistedItem];
      newData.push(payload);
      return {
        ...state,
        shortlistedItem: newData,
      };
    default:
      return state;
  }
};

export default homeListReducer;
