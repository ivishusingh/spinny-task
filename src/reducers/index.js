import { combineReducers } from 'redux';
import homeListReducer from './homeList';

const rootReducer = combineReducers({
  homeListReducer,
});

export default rootReducer;
